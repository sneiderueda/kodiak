const { Router } = require ('express');

const router = Router();
const conceptosCtrl = require('../controllers/conceptos.controller')

router.get('/',conceptosCtrl.getConceptos)
router.post('/',conceptosCtrl.createConcepto)
router.get('/:id',conceptosCtrl.getConcepto)
router.put('/:id',conceptosCtrl.updateConcepto)
router.delete('/:id',conceptosCtrl.deleteConcepto)


module.exports = router;
