const conceptoModel = require('../models/conceptos.model')


const conceptosCtrl={}

//FUNCIONES RUTAS
conceptosCtrl.getConceptos = async(req, res) => {
    
    const conceptos = await conceptoModel.find()
    const prueba = "<h1>Prueba</h1>";
    res.json(conceptos)
   

}

conceptosCtrl.createConcepto = async (req, res) => {
    
    const newConcepto = new conceptoModel (req.body)
    await newConcepto.save();
    res.send({message :'Registro Almacenado'})
    
}

conceptosCtrl.getConcepto = async (req, res) => {
   
    const concepto = await conceptoModel.findById(req.params.id)
    res.json(concepto)

}

conceptosCtrl.updateConcepto = async (req, res) => {

    await conceptoModel.findByIdAndUpdate(req.params.id,req.body)
    res.json({message:'Registro actualizado'})

}

conceptosCtrl.deleteConcepto = async (req, res) => {

    await conceptoModel.findByIdAndDelete(req.params.id)
    res.send({message :'Registro Eliminado'})

}

module.exports = conceptosCtrl;