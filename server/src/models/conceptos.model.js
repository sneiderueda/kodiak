const {Schema, model} = require ('mongoose')

//Modelo
const conceptoSchema = new Schema ({
    codigo: {type:Number, require},
    concepto: {type:String, require},
},{
    timestamps: true,
    versionKey:false
})

module.exports = model('conceptoModel',conceptoSchema);