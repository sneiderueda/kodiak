const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();

//VARIABLES DE ENTORNO
app.set('port',process.env.PORT || 3000);

//MIDDELWARES
app.use(morgan('dev')); 
app.use(cors()); //origin: "http://---" si se quiere que acepte solo peticiones de un servidor
app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use("/kodiak/conceptos",require('./routes/conceptos.routes'));


module.exports = app;