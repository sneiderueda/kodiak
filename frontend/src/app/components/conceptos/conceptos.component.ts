import { Component, OnInit } from '@angular/core';
import { ConceptoService } from "../../services/concepto.service";
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-conceptos',
  templateUrl: './conceptos.component.html',
  styleUrls: ['./conceptos.component.css']
})

export class ConceptosComponent implements OnInit {

  constructor(public conceptoService: ConceptoService) {

  }

  ngOnInit(): void {
    this.getConceptos();
  }

  getConceptos() {

    this.conceptoService.getConceptos().subscribe({
        next: (res) => 
        //console.log(res),
        this.conceptoService.conceptos = res,
        error: (err) => console.log(err)
      })
  }

  addConcepto(form: NgForm){
    console.log(form.value)
  }

}
