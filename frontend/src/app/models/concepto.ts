export interface concepto {
    _id: string, 
    codigo: number, 
    concepto: string, 
    createdAt: string, 
    updatedAt: string
}