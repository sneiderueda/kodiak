import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { concepto } from "../models/concepto";

@Injectable({
  providedIn: 'root'
})

export class ConceptoService {

  URL_API = 'http://localhost:3000/kodiak/conceptos/';

  
  conceptos: concepto[];
  selectedConcepto: concepto;

  constructor(private http: HttpClient) {
  
    
  }

  getConceptos() {

    return this.http.get<concepto[]>(this.URL_API);

  }

  getConcepto() {

  }

}
