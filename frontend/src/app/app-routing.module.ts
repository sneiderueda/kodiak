import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Importación de Componentes
import { InicioComponent } from "../app/components/inicio/inicio.component";
import { ConceptosComponent } from "../app/components/conceptos/conceptos.component";

const routes: Routes = [
  {
    path: '',
    component: InicioComponent
  },
  {
   path: "conceptos",
   component: ConceptosComponent
  },
  {
    path: "**",
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
